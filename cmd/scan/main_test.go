package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestValidateAIClientData(t *testing.T) {
	tests := []struct {
		name     string
		apiV4Url string
		jobID    string
		token    string
		wantErr  bool
	}{
		{"valid data", "http://127.0.0.1:3000/api/v4", "1", "secret", false},
		{"url missing", "", "1", "secret", true},
		{"jobID missing", "http://127.0.0.1:3000/api/v4", "", "secret", true},
		{"token missing", "http://127.0.0.1:3000/api/v4", "1", "", true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testApp := application{
				aiclientData: struct {
					apiV4Url string
					jobID    string
					token    string
				}{tt.apiV4Url, tt.jobID, tt.token},
			}

			gotErr := testApp.validateAIClientData()
			if tt.wantErr {
				require.Error(t, gotErr)
			} else {
				require.NoError(t, gotErr)
			}
		})
	}
}
